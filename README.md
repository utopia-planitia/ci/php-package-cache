# php package cache

A deployment of [Cito](https://github.com/damoon/cito-server) to speed up and stablize php builds.

## Access

The [Gitlab runner](https://gitlab.com/utopia-planitia/gitlab-kubernetes-runner) checks for composer and installs the [Cito plugin](https://github.com/damoon/cito).
