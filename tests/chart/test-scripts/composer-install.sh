#!/bin/bash
set -euxo pipefail

mkdir /workspace
cd /workspace

# add allow-plugins configuration if it is supported by composer (depends on the version)
if composer config --global allow-plugins >/dev/null 2>&1; then
    composer global config --no-interaction -- allow-plugins.repman-io/composer-plugin true
fi

composer global require repman-io/composer-plugin

cp /tests/composer.json /tests/composer.lock .

time composer install --ignore-platform-reqs --no-scripts --no-autoloader
